import * as am4core from "@amcharts/amcharts4/core";
// import * as am4charts from "@amcharts/amcharts4/charts";
// import * as am4plugins_forceDirected from "@amcharts/amcharts4/plugins/forceDirected";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

// import * as am4core from "@amcharts/amcharts4/core";
import * as am4plugins_forceDirected from "@amcharts/amcharts4/plugins/forceDirected";
   
export default function App(){

am4core.useTheme(am4themes_animated);

// Create chart
var chart = am4core.create("chartdiv", am4plugins_forceDirected.ForceDirectedTree);

// Create series
var series = chart.series.push(new am4plugins_forceDirected.ForceDirectedSeries());

// Icons
var workstation = "M0 30l4-8h24l4 8zM4 2v18h24V2H4zm22 16H6V4h20v14z";
var server = "M6 0v30h2v2h4v-2h8v2h4v-2h2V0H6zm6 6H8V4h4v2z";
var printer = "M28 26h2V16h-4V2H6v14H2v10h2l-2 4h28l-2-4zM8 4h16v12H8V4zm0 20h16l2 4H6l2-4z";
var router = "M26 20v-8a5 5 0 1 0-2 0v8l-2 1-11-11a5 5 0 0 0-4-8 5 5 0 0 0-1 10v8a5 5 0 1 0 2 0v-8l2-1 11 11-1 3a5 5 0 1 0 6-5z";

// Set data
series.data = [{
  "name": "Network #1",
  "path": server,
  "children": [{
    "name": "Workstation #1",
    "value": 1,
    "path": workstation
  }, {
    "name": "Workstation #2",
    "value": 1,
    "path": workstation
  }, {
    "name": "Workstation #3",
    "value": 1,
    "path": workstation
  }, {
    "name": "Workstation #4",
    "value": 1,
    "path": workstation
  }, {
    "name": "Printer",
    "value": 1,
    "path": printer
  }, {
    "name": "Bridge",
    "value": 1,
    "path": router,
    "link": ["n2"]
  }]
}, {
  "name": "Network #2",
  "path": server,
  "children": [{
    "name": "Workstation #1",
    "value": 1,
    "path": workstation
  }, {
    "name": "Workstation #2",
    "value": 1,
    "path": workstation
  }, {
    "name": "Workstation #3",
    "value": 1,
    "path": workstation
  }, {
    "id": "n2",
    "name": "Bridge",
    "value": 1,
    "path": router
  }]
}];
// Set up data fields
series.dataFields.value = "value";
series.dataFields.name = "name";
series.dataFields.id = "id";
series.dataFields.children = "children";
series.dataFields.linkWith = "link";

// Add labels
series.nodes.template.label.text = "{name}";
series.nodes.template.label.valign = "bottom";
series.nodes.template.label.fill = am4core.color("#000");
series.nodes.template.label.dy = 10;
series.nodes.template.tooltipText = "{name}: [bold]{value}[/]";
series.fontSize = 10;
series.minRadius = 30;
series.maxRadius = 30;

// Configure circles
series.nodes.template.circle.fill = am4core.color("#fff");
series.nodes.template.circle.fill = am4core.color("#fff");

// Configure icons
var icon = series.nodes.template.createChild(am4core.Sprite);
icon.propertyFields.path = "path";
icon.horizontalCenter = "middle";
icon.verticalCenter = "middle";

series.centerStrength = 0.4;


}






// import React, { useState, useEffect } from "react";

// import "./App.css";


// const App = () => {

//   const [todos, setTodos] = useState([
//     { taskID: 1, task: "Walk the walk" },
//     { taskID: 2, task: "Talk the talk" },
//     { taskID: 3, task: "Jump the jump" }
//   ]);

//   const [progressTasks, setProgressTasks] = useState([]);
//   const [testingTasks, setTestingTasks] = useState([]);
//   const [completedTasks, setCompletedTasks] = useState([]);
//   const [draggedTask, setDraggedTask] = useState({});
//   const [from, setFrom] = useState("");


//   useEffect(()=>{
//     console.log(888, todos);
//   },[]) 


//   const onDrag = (event, todo, from) => {
//     event.preventDefault();
//     setDraggedTask(todo);
//     setFrom(from);
//   };

//   const onDragOver = (event) => {
//     event.preventDefault();
//   };

//   const onDropTodo = () => {
//     setTodos([...todos, draggedTask]);
//     sliceData();
//   };

//   const onDropProgresh = () => {
//     setProgressTasks([...progressTasks, draggedTask]);
//     sliceData();
//   };

//   const onDropTesting = () => {

//     setTestingTasks([...testingTasks, draggedTask]);

//     sliceData();

//   };

//   const onDropComplete = () => {

//     setCompletedTasks([...completedTasks, draggedTask])
//     sliceData();

//   };

//   const sliceData = () => {

//     if (from == "todo") {
//       setTodos(todos.filter((task) => task.taskID !== draggedTask.taskID))
//     }
//     else if (from == "progress") {

//       setProgressTasks(progressTasks.filter((task) => task.taskID !== draggedTask.taskID));
//     }
//     else if (from == "testing") {
//       setTestingTasks(testingTasks.filter((task) => task.taskID !== draggedTask.taskID))
//     }
//     else if (from == "completed") {
//         setCompletedTasks(completedTasks.filter((task) => task.taskID !== draggedTask.taskID))
//         setDraggedTask({})
//     }

//   };

//     return (

//       <div className="App">

//         <div

//           className="todos"

//           onDrop={(event) => onDropTodo(event)}

//           onDragOver={(event) => onDragOver(event)}

//         >

//           {todos.map((todo) => (

//             <div

//               key={todo.taskID}

//               draggable

//               onDrag={(event) => onDrag(event, todo, "todo")}

//             >

//               {todo.task}

//             </div>

//           ))}

//         </div>

//         <div

//           onDrop={(event) => onDropProgresh(event)}

//           onDragOver={(event) =>onDragOver(event)}

//           className="done"

//         >

//           {progressTasks.map((task) => (

//             <div

//               key={task.taskID}

//               draggable

//               onDrag={(event) => onDrag(event, task, "progress")}

//             >

//               {task.task}

//             </div>

//           ))}

//         </div>

//         <div

//           onDrop={(event) => onDropTesting(event)}

//           onDragOver={(event) => onDragOver(event)}

//           className="done"

//         >

//           {testingTasks.map((task, index) => (

//             <div

//               key={task.taskID}

//               draggable

//               onDrag={(event) => onDrag(event, task, "testing")}

//             >

//               {task.task}

//             </div>

//           ))}

//         </div>


//         <div

//           onDrop={(event) => onDropComplete(event)}

//           onDragOver={(event) => onDragOver(event)}

//           className="done"

//         >

//           {completedTasks.map((task) => (

//             <div

//               key={task.taskID}

//               draggable

//               onDrag={(event) => onDrag(event, task, "completed")}

//             >

//               {task.task}

//             </div>

//           ))}

//         </div>

//       </div>

//     );

//   }

//   export default App;





// memo
// import { memo, useState } from 'react';

// export default function MyApp() {
//   const [name, setName] = useState('');
//   const [address, setAddress] = useState('');
//   return (
//     <>
//       <label>
//         Name{': '}
//         <input value={name} onChange={e => setName(e.target.value)} />
//       </label>
//       <label>
//         Address{': '}
//         <input value={address} onChange={e => setAddress(e.target.value)} />
//       </label>
//       <Greeting name={name} />
//     </>
//   );
// }

// const Greeting = memo(function Greeting({ name }) {
//   console.log("Greeting was rendered at", new Date().toLocaleTimeString());
//   return <h3>Hello{name && ', '}{name}!</h3>;
// });



// useCallback and useMemo

// import React, { useState, useMemo, useCallback } from 'react';

// const MyComponent = () => {
// const [count, setCount] = useState(0);

// // Example 1: useMemo
// const expensiveResult = useMemo(() => {
// // Perform an expensive computation based on count
// console.log('Computing expensive result...');
// return count * 2;
// }, [count]);

// // Example 2: useCallback
// const handleClick = useCallback(() => {
// // Handle click event
// console.log('Button clicked!');
// setCount(count + 1);
// }, [count]);

// return (
// <>
// <h1>Count: {count}</h1>
// <h2>Expensive Result: {expensiveResult}</h2>
// <button onClick={handleClick}>Increment Count</button>
// </>
// );
// };

// export default MyComponent;



// useMemo
// const LearnUseMemo = () => {
//   const [count, setCount] = useState(1);
//   const [item, setItem] = useState(5);

//   const expensiveFunction = (num) => {
//     console.log("expensive function called");
//     let val = 1;
//     for (let i = 1; i <= 100; i++) {
//       val = parseInt(num) - 1;
//     }
//     return val;
//   }

//   const FinalValue =useMemo(()=>
//     {
// return expensiveFunction(count);
//     }
//     , [count])

//     return(
//       <>
//       <h1>useMemo Hook</h1>
//       <h3>count value is: {count}</h3>
//       <h3>item value is: {item}</h3>
//       <h3>expensive value is: {FinalValue}</h3>
//       <button onClick={()=>setCount(count+1)}>Increase Count</button>
//       <button onClick={()=>setItem(item+1)}>Increase Item</button>
//       </>
//     )
// }
// export default LearnUseMemo;


//intercept code
// import React, { useEffect, useState } from 'react';
// import axios from 'axios';
// import './App.css';
// const App = () => {
//    const [data, setData] = useState(null);
//    const [val, setVal] = useState('');
//    const [fetchData, setFetch] = useState(false);

//    useEffect(() => {

//       if (fetchData) {
//          const payload = {
//             method: 'POST',
//             body: JSON.stringify({ title: val }),
//          };
//          axios.post('https://jsonplaceholder.typicode.com/posts', payload)
//             .then((res) => setData(res.data.id));
//       }
//    }, [fetchData]);
//    return (
//       <>
//          {data && <h1>Your data is saved with Id: {data}</h1>}
//          <input
//             placeholder="Title of Post"
//             value={val}
//             onChange={(e) => setVal(e.target.value)} />

//          <button onClick={() => setFetch(true)}>Save Data</button>
//       </>
//    );
// };
// export default App;






